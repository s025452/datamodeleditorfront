import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css']
})
export class CrudComponent implements OnInit {
  chemin: string;
  nom: string;

  constructor( private route: ActivatedRoute,  private router: Router) { }

  ngOnInit() {
    this.route.firstChild.params.subscribe(routeParams => {
      this.chemin = routeParams['chemin'];
      this.nom = routeParams['nom'];
      
      
    });

  }
  explorer() {
    this.router.navigate(['/explorer', { ressource:  this.chemin }]);
  }
  select() {
    this.router.navigate(['/crud/retrieve', { chemin: this.chemin , nom:this.nom }]);
  }
  create() {
    this.router.navigate(['/crud/create', { chemin: this.chemin , nom:this.nom }]);
  }
}
