import { Component, OnInit, Input } from '@angular/core';


export interface APIRest {
    nom:string
    requete:string
    erreur?:string,
    useIndex?:boolean ,
    mode?:string

}
@Component({
  selector: 'app-api-rest',
  templateUrl: './api-rest.component.html',
  styleUrls: ['./api-rest.component.css']
})
export class ApiRestComponent implements OnInit {
  @Input()
  apiRest:APIRest;
  constructor() { }

  ngOnInit() {
  }

}
