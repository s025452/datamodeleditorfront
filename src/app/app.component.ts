import { Component, OnInit, Input } from '@angular/core';
import { TypeService, Arbre, ConfigArbre, Objet, Propriete, ValeurString, ValeurBoolean, Tableau, ValeurNombre } from 'dadou-tree';
import { ConfigTypes, ConfigDefaut } from './config.types';
import { Type } from 'generator-666';

import * as env from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @Input()
  chemin  = '';
  @Input()
  extensions = ['.model.json','.crud.json'];
    constructor(private http: HttpClient, private router: Router ) {
  

  }

  ngOnInit() {


  }



}
