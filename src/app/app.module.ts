import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {TreeModule} from 'dadou-tree';
import {FormsModule } from '@angular/forms';
import { ExplorateurComponent } from './explorateur/explorateur.component';
import { SelectComponent } from './select/select.component';
import { EditorComponent } from './editor/editor.component';
import { JsonObjectComponent } from './json-object/json-object.component';
import { JsonArrayComponent } from './json-array/json-array.component';
import { CrudComponent } from './crud/crud.component';
import { CreateComponent } from './create/create.component';
import { ApiRestComponent } from './api-rest/api-rest.component';
import { ApiRestListComponent } from './api-rest-list/api-rest-list.component';
import { ModelComponent } from './model/model.component';
import { ConfigComponent } from './config/config.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    ExplorateurComponent,
    SelectComponent,
    EditorComponent,
    JsonObjectComponent,
    JsonArrayComponent,
    CrudComponent,
    CreateComponent,
    ApiRestComponent,
    ApiRestListComponent,
    ModelComponent,

    ConfigComponent
  ],
  imports: [
    TreeModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxSpinnerModule,
    BrowserAnimationsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
