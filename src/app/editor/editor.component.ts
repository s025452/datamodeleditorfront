import { HttpClient } from '@angular/common/http';
import { Component, OnInit, OnChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as env from '../../environments/environment';
@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit{
  chemin: string;
  nom: string;
  generationRep: string

  constructor( private http: HttpClient,private route: ActivatedRoute,  private router: Router) { }

  ngOnInit() {
    this.route.firstChild.params.subscribe(routeParams => {
      this.chemin = routeParams['chemin'];
      this.nom = routeParams['nom'];
      this.http.get(`${env.AppConfig.host}/model-association`).subscribe((data: any) => {
        var ressource: string  = this.chemin+"/"+this.nom
        ressource = ressource.split("\\").join("/");
        this.generationRep = data[ressource]
          
        });
      
    });

  }
  generer() {

  }
  config() {
    this.router.navigate(['/editor/config', { chemin: this.chemin , nom:this.nom }]);
  }
  explorer() {
    this.router.navigate(['/explorer', { ressource:  this.chemin }]);
  }
  model() {
    this.router.navigate(['/editor/model', { chemin: this.chemin , nom:this.nom }]);
  }
  api() {
    this.router.navigate(['/editor/api', { chemin: this.chemin , nom:this.nom }]);
  }
}
