import { Component, OnInit, OnChanges } from '@angular/core';
import { TypeService, Arbre, ConfigArbre, Objet, Propriete, ValeurString, ValeurBoolean, Tableau, ValeurNombre } from 'dadou-tree';
import { ConfigTypes, ConfigDefaut } from '../config.types';
import { Type } from 'generator-666';

import * as env from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.css']
})
export class ModelComponent implements OnInit {
  chemin: string;
  ressource: string = '';
  valeur: string;
  obj: any;
  extensions = ['.model.json'];
  racine = "Types";
  contenu = '[]';
  types: Type[] = [
    {
      nom: 'Colonne',
      abstrait: false,
      champs: [
        { nom: 'nom', type: 'string' }
      ]
    },
    {
      nom: 'Index',
      abstrait: false,
      champs: [
        { nom: 'nom', type: 'string' },
        { nom: 'unique', type: 'boolean' },
        { nom: 'colonnes', type: '*Colonne' }
      ]
    },
    {
      nom: 'Types',
      abstrait: false,
      champs: [
        { nom: 'types', type: '*TypeDefRacine' }
      ]
    },
    {
      nom: 'Type',
      abstrait: true
    },
    {
      nom: 'TypePersistant',
      super: 'Type',
      abstrait: true,
      champs: [
        { nom: 'notNull', type: "boolean" }
      ]
    },

    {
      nom: 'TypeRef',
      super: 'TypePersistant',
      abstrait: false,
      champs: [
        { nom: 'nom', type: 'string' },
        { nom: 'noInsert' , type:'boolean'} ,
        { nom: 'noUpdate' , type:'boolean'}
      ]
    },
    {
      nom: 'TypeList',
      super: 'Type',
      abstrait: false,
      champs: [
        { nom: 'nom', type: 'string' },
        { nom: 'lienInverse', type: 'string' }
      ]
    },
    {
      nom: 'TypeListRelation',
      super: 'Type',
      abstrait: false,
      champs: [
        { nom: 'nom', type: 'string' },
        { nom: 'relation', type: 'string' }
      ]
    },
    {
      nom: 'TypeChaine',
      super: 'TypePersistant',
      abstrait: false
    },
    {
      nom: 'TypeEntier',
      super: 'TypePersistant',
      abstrait: false
    },
    {
      nom: 'TypeDate',
      super: 'TypePersistant',
      abstrait: false
    },
    {
      nom: 'TypeNombre',
      super: 'TypePersistant',
      abstrait: false
    },
    {
      nom: 'TypeBoolean',
      super: 'TypePersistant',
      abstrait: false
    },
    {
      nom: 'Champ',
      abstrait: false,
      champs: [
        { nom: 'nom', type: 'string' },
        { nom: 'type', type: 'Type' }
      ]

    },
    {
      nom: 'TypeDef',
      abstrait: false,
      champs: [
        { nom: 'nom', type: 'string' },
        { nom: 'persist', type: 'boolean' },
        { nom: 'abstrait', type: 'boolean' },
        { nom: 'champs', type: '*Champ' },
        { nom: 'indexes', type: '*Index' },
        { nom: 'sousTypes', type: '*TypeDef' }
      ],
    },
    {
      nom: 'TypeDefRacine',
      abstrait: false,
      champs: [
        { nom: 'nom', type: 'string' },
        { nom: 'persist', type: 'boolean' },
        { nom: 'typeClef', type: 'Identifiant' },
        { nom: 'genererClef', type: 'boolean' },
        { nom: 'abstrait', type: 'boolean' },
        { nom: 'champs', type: '*Champ' },
        { nom: 'indexes', type: '*Index' },
        { nom: 'sousTypes', type: '*TypeDef' }
      ],
    },
    {
      nom: 'Identifiant',
      abstrait: true
    },
    {
      nom: 'Chaine',
      super: 'Identifiant',
      abstrait: false
    },
    {
      nom: 'Entier',
      super: 'Identifiant',
      abstrait: false
    }
  ];
  typeService: TypeService;
  configArbre: ConfigArbre;
  objet: Objet;
  afficherEnregistrer: boolean = false;
  afficherInstaller: boolean = false;
  message = ' en attente ...';
  config: any
  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router, private spinner: NgxSpinnerService) {
    this.typeService = new TypeService();
    this.typeService.init(this.types);
    const configType = new ConfigTypes();

    configType.typeService = this.typeService;
    this.configArbre = configType;
    this.objet = this.typeService.creerObjet(this.racine);
    this.http.get(`${env.AppConfig.host}/config`).subscribe((data: any) => {
      if (data.jvm) {
        this.afficherEnregistrer = true;
      }
      if (data.serveurEnvironement && data.keyEnvironement) {
        this.afficherInstaller = true;
      }
      this.config = data
    });


  }

  ngOnChanges() {


  }
  nom: string
  erreurs: any []
  adminKey: string 
  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.chemin = routeParams['chemin'];
      this.nom = routeParams['nom'];
      this.erreurs = []
      const s = routeParams['erreurs']
      this.adminKey = routeParams['adminKey']
      if (s) {
      this.erreurs = JSON.parse(routeParams['erreurs']); }
      const event = this.chemin + '/' + this.nom;
      this.charger(event);
    });

  }


  charger(ressource: string) {

    this.ressource = ressource;

    this.http.post(`${env.AppConfig.host}/lire`, ressource).subscribe(
      (data: any) => {
        const ls: any[] = []
        if (!data.data) {
          this.objet = this.typeService.creerObjet(this.racine);
          return;
        }

        this.obj = JSON.parse(data.data);

        this.objet = this.creerArbre(this.racine, this.obj.model) as Objet;


      }
    )
  }
  creerArbre(typeNom: string, obj: any): Arbre {
    if (obj === undefined || obj === null) {
      return undefined;
    }
    var type: Type = this.types.find((t) => t.nom === typeNom);
    if (type) {
      const sousTypes: Type[] = this.typeService.sousTypesIndirect(typeNom);
      if (sousTypes.length === 1) {
        const r: Objet = new Objet();
        r.typeBase = typeNom;
        r.type = typeNom;
        r.estOuvert = true;
        this.creerObjet(type, obj, r);
        return r;
      }
      const r: Objet = new Objet();
      r.typeBase = typeNom;
      r.type = obj.type;
      if (!obj.type) {
        return undefined;
      }
      r.estOuvert = false;
      type = this.types.find((t) => t.nom === obj.type);
      this.creerObjet(type, obj.valeur, r);
      return r;
    }
    if (typeNom === 'number') {
      return new ValeurNombre(obj);
    }
    if (typeNom === 'string') {
      return new ValeurString(obj);
    }
    if (typeNom === 'boolean') {
      return new ValeurBoolean(obj === true);
    }
    if (typeNom.startsWith("*")) {
      const tableau: Tableau = new Tableau();
      tableau.estOuvert = true;
      tableau.typeBase = typeNom.substring(1);
      obj.forEach((element: any) => {
        const elt = this.creerArbre(typeNom.substring(1), element);
        elt.tableauParent = tableau;

        tableau.valeurs.push(elt);
        if (elt instanceof Objet) {
          elt.estOuvert = false;
        }
      })
      return tableau;

    }
    return undefined;


  }

  creerObjet(type: Type, obj: any, objet: Objet) {
    if (type.super) {
      const superType: Type = this.types.find((t) => t.nom === type.super);

      this.creerObjet(superType, obj, objet);
    }
    if (type.champs) {
      type.champs.forEach((champ) => {
        objet.ajouter(champ.nom, this.creerArbre(champ.type, obj[champ.nom]));

      })
    }

  }
  installer() {
    this.enregistrer( ( obj: any )=> {
      this.http.post(`${this.config.serveurEnvironement}/install`, {
        adminKey:this.config.keyEnvironement,
        modelKey: obj.modelKey ,
        model:obj
      }).subscribe(
        (data: any)=> {
          obj.modelKey = data.modelKey
          this.http.post(`${env.AppConfig.host}/ecrire`, {
            ressource: this.ressource,
            type: 'file',
            contenu: JSON.stringify(obj)
          }).subscribe((data: any) => { })       
        },
        (erreur: any)=> {

        }

      )


    })
  }

  enregistrer( action:( obj: any )=>void) {

    const obj = this.typeService.convertirArbre(this.objet);
    if (obj) {
      this.obj.model = obj;
    } else {
      this.obj = { model: obj, apis: [] }
    }
    this.spinner.show();
    this.http.post(`${env.AppConfig.host}/ecrire`, {
      ressource: this.ressource,
      type: 'file',
      contenu: JSON.stringify(this.obj)
    }).subscribe((data: any) => {
      this.http.post(`${env.AppConfig.host}/generer_client`, {
        ressource: this.ressource
      }).subscribe((data2: any) => {
        this.erreurs = data2.erreurs;
        this.adminKey = data2.adminKey;
        console.log("resulta" + data2.erreurs)
        if (data2.erreurs.length === 0 && action) {
          action(this.obj);

        }
        this.spinner.hide();

      },
        (error: any) => {
          this.spinner.hide();

          this.router.navigate(['/editor/config', { chemin: this.chemin, nom: this.nom }]);
        })


    });
    return;



  }

}
