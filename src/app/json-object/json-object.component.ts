import { Component, OnInit, Input } from '@angular/core';
import { Objet } from 'dadou-tree';

@Component({
  selector: 'app-json-object',
  templateUrl: './json-object.component.html',
  styleUrls: ['./json-object.component.css']
})
export class JsonObjectComponent implements OnInit {
  @Input()
  object: any 
  open= true;
  constructor() { }
  toggle() {
    this.open = ! this.open;
  }
  label() {
    if (this.open) {
      return "-";
    }
    return "+";
  }
  ngOnInit() {
  }
  keys( ) {
    return Object.keys(this.object);
  }
  
  isArray( obj:any): boolean {
    return Array.isArray(obj);
  }
  isObject( obj:any): boolean {
    return  (typeof obj !== "number" && typeof obj !== "boolean" && typeof obj !== "string" && !Array.isArray(obj))
  }
  isNumber( obj:any):boolean {
    return typeof obj === "number";
  }
  isBoolean( obj:any):boolean {
    return typeof obj === "boolean";
  }
  isString( obj:any):boolean {
    return typeof obj === "string";
  }
}
