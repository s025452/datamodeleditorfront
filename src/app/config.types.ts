import { ConfigArbre, Arbre, Objet, Tableau, TypeService, Chemin, ValeurString, Feuille, Propriete } from 'dadou-tree';


export class ConfigDefaut extends ConfigArbre {
  afficher(noeud: Arbre): string {
    if (noeud instanceof Objet) {
      return noeud.type;
    }
  }
  estTable(tableau: Tableau, typeService: TypeService): boolean {
    const type = typeService.types.find((type) => { return type.nom === tableau.typeBase });

    if (type.champs) {
      const champ = type.champs.find((c) => { return c.type !== 'number' && c.type != 'string' && c.type != 'boolean' && c.type != 'int' });
      if (champ) {
        return false;
      }
    }
    return true;

  }
}

export class ConfigTypes extends ConfigArbre {
  typeService:TypeService;
  afficher(noeud: Arbre): string {
    if (noeud instanceof Objet) {
      if (noeud.estOuvert) {
        return null;
      }
      const prop: Propriete = noeud.proprietes.find((c) => { return c.nom === 'nom' });
      if (prop) {
        if (prop.valeur instanceof ValeurString) {
          return prop.valeur.contenu;
        }
      }

      return noeud.type;
    }
    return null;
  }
  estTable(tableau: Tableau, typeService: TypeService): boolean {
    if (tableau.typeBase === 'TypeDef') {
      return false;
    }
    if (tableau.typeBase === 'TypeDefRacine') {
      return false;
    }

    if (tableau.typeBase === 'Index') {
      return false;
    }
    return true;

  }
  donnerTypeRelationPourArbre( racine: Arbre ): Map<string,Map<string,number>> {
    const obj = this.typeService.convertirArbre(racine);
    const r = new Map<string,Map<string,number>>();
    if (obj.types) {
      obj.types.forEach(element => {
        this.donnerTypeRelationPourObj(element, null , r);  

      });

    
  }
    return r;
    
  }
  donnerTypeRelationPourObj( obj: any  ,parent:string, map:Map<string,Map<string,number>>) {
    const types:Map<string,number> = new Map();
    if (parent) {
  
    map.get(parent).forEach ( (val , key)=> {

      types.set(key,val);
    }); 
  }
    if (obj.champs) {
    obj.champs.forEach( (champ) => {
      if (champ.type) {
        if (champ.type.type === 'TypeRef') {
          const nom: string = champ.type.valeur.nom ; 
          var n = 1;
          if (types.has(nom)) {
            n = types.get(nom) + 1;
          }
          types.set(nom,n);
        }
      }
    }); }
    map.set(obj.nom , types);
    if (obj.sousTypes) {
      obj.sousTypes.forEach((element:any) => {
        this.donnerTypeRelationPourObj(element , obj.type , map);        
      });
    }
    
  }
  nomTypes(racine: Arbre, noms: string[]) {
    if (racine instanceof Objet) {
      racine.proprietes.forEach(prop => {
        if (prop.nom === 'types') {
          if (prop.valeur instanceof Tableau) {
            prop.valeur.valeurs.forEach(element => {
              this.nomTypes(element, noms);
            });
          }
        }
        if (prop.nom === 'nom') {
          if (prop.valeur instanceof ValeurString) {
            noms.push(prop.valeur.contenu);
          }
        }
        if (prop.nom === 'sousTypes') {
          if (prop.valeur instanceof Tableau) {
            prop.valeur.valeurs.forEach(arbre => {
              this.nomTypes(arbre, noms);
            });
          }
        }
      });
    }
  }
  donnerNom(objet: Objet): string {
    const arbre = this.donnerArbre(objet, 'nom');
    if (arbre instanceof ValeurString) {
      return arbre.contenu;
    }
    return null;
  }
  donnerArbre(objet: Arbre, nom: string): Arbre {
    if (objet instanceof Objet) {
      const prop = objet.proprietes.find((elt) => {
        return elt.nom === nom;
      });
      if (prop) {
        return prop.valeur;
      }
    }
    return null;
  }
  donnerChaine(objet: Arbre, nom: string): string {
    const arbre = this.donnerArbre(objet, nom);
    if (arbre instanceof ValeurString) {
      return arbre.contenu;
    }
    return null;
  }
  donnerChamps(objet: Objet, noms: string[]) {
    const arbre = this.donnerArbre(objet, 'champs');
    if (arbre instanceof Tableau) {
      arbre.valeurs.forEach((elt) => {
        const nom = this.donnerChaine(elt, 'nom');
        const arbre = this.donnerArbre(elt, 'type');
        if (arbre instanceof Objet) {
          if (arbre.type !== 'TypeList') {
            noms.push(nom);
          }
        }
      });

    }

  }
  donnerChampsPourType(objet: Objet,type:string, noms: string[]) {
    const arbre = this.donnerArbre(objet, 'champs');
    if (arbre instanceof Tableau) {
      arbre.valeurs.forEach((elt) => {
        const nom = this.donnerChaine(elt, 'nom');
        const arbre = this.donnerArbre(elt, 'type');
        if (arbre instanceof Objet) {
          if (arbre.type == 'TypeRef'  && this.donnerChaine(arbre, 'nom') === type) 
          {
            noms.push(nom);
          }
        }
      });

    }

  }
  colonnes(racine: Arbre, noms: string[], nom: string): boolean {
    if (racine instanceof Objet) {
      const arbre = this.donnerArbre(racine, 'types');
      if (arbre && arbre instanceof Tableau) {
        arbre.valeurs.forEach(element => {
          this.colonnes(element, noms, nom);
        });
        return

      }
      const nomType = this.donnerChaine(racine, 'nom');
      const sousTypes = this.donnerArbre(racine, 'sousTypes');
      if (nomType) {
        if (nomType === nom) {
          this.donnerChamps(racine, noms);
          return true;
        } else {
          if (sousTypes instanceof Tableau) {
            sousTypes.valeurs.forEach((elt) => {
              if (this.colonnes(elt, noms, nom)) {
                this.donnerChamps(racine, noms);
              }
            })
          }
        }
      }
    }
    return false;

  }
  colonnesAvecType(racine: Arbre,type:string , noms: string[], nom: string): boolean {
    if (racine instanceof Objet) {
      const arbre = this.donnerArbre(racine, 'types');
      if (arbre && arbre instanceof Tableau) {
        arbre.valeurs.forEach(element => {
          this.colonnesAvecType(element,type, noms, nom);
        });
        return

      }
      const nomType = this.donnerChaine(racine, 'nom');
      const sousTypes = this.donnerArbre(racine, 'sousTypes');
      if (nomType) {
        if (nomType === nom) {
          this.donnerChampsPourType(racine,type, noms);
          return true;
        } else {
          if (sousTypes instanceof Tableau) {
            sousTypes.valeurs.forEach((elt) => {
              if (this.colonnesAvecType(elt,type, noms, nom)) {
                this.donnerChampsPourType(racine,type, noms);
              }
            })
          }
        }
      }
    }
    return false;

  }
  choix(racine: Arbre, chemin: Chemin): string[] {
    if (chemin.parent) {
      if (chemin.parent.arbre instanceof Objet) {
        if (chemin.parent.arbre.type === 'TypeRef') {
          const ls: string[] = [];
          this.nomTypes(racine, ls);
          return ls;
        }
        if (chemin.parent.arbre.type === 'TypeList' && chemin.valeur =='nom') {

          const ls: string[] = [];
          this.nomTypes(racine, ls);
          return ls;
        }
        if (chemin.parent.arbre.type === 'TypeList' && chemin.valeur =='lienInverse') {
          
          const ls: string[] = [""];
          const type = this.donnerChaine(chemin.parent.arbre,"nom");
          const typeReference = this.donnerChaine(chemin.parent.parent.parent.parent.arbre,"nom");
          this.colonnesAvecType(racine,typeReference,ls,type)
          //this.nomTypes(racine, ls);
          return ls;
        }
        if (chemin.parent.arbre.type === 'TypeListRelation' && chemin.valeur =='nom') {
          

          const ls: string[] = [];
          this.nomTypes(racine, ls);
          return ls;          //this.nomTypes(racine, ls);
          
        }
        if (chemin.parent.arbre.type === 'TypeListRelation' && chemin.valeur =='relation') {
          
          const ls: string[] = [""];
          const map = this.donnerTypeRelationPourArbre( racine );
          const type = this.donnerChaine(chemin.parent.arbre,"nom");
          const typeReference = this.donnerChaine(chemin.parent.parent.parent.parent.arbre,"nom");

          map.forEach( (value,key)=> {
            if (type === typeReference) {
                if (value.get(type) === 2) {
                  ls.push(key);    
                }
            }else 
            if (value.get(type) === 1 && value.get(typeReference) === 1) {
              ls.push(key);
            }
          
          });
          return ls;
        }
      }
      if (chemin.valeur === 'nom' && chemin.parent.parent.arbre instanceof Tableau) {
        if (chemin.parent.parent.arbre.typeBase === 'Colonne') {
          const arbre = chemin.parent.parent.parent.parent.parent.arbre;
          if (arbre instanceof Objet) {
            const nom = this.donnerChaine(arbre, 'nom');
            const noms: string[] = [];
            this.colonnes(racine, noms, nom);
            return noms;
          }
          return ["??"];
        }
        return null;



      }

    }
  }
}
