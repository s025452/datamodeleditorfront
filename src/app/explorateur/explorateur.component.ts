import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as env from '../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

interface Element {
  name: string;
  type: string;
}
interface ElementNouveau {
  name: string;
  type: string;
  valider: boolean;
}
interface Parent {
  chemin: string;
  parent?: Parent;
}

@Component({
  selector: 'app-explorateur',
  templateUrl: './explorateur.component.html',
  styleUrls: ['./explorateur.component.scss']
})

export class ExplorateurComponent implements OnInit {
  @Input()
  chemin  = '';
  @Input()
  extensions = ['.model.json'];


  @Input()
  contenu: string ;

  parent: Parent;

  nouveau: ElementNouveau;


  modelCourant: string

  elements: Element[] = [];
  erreurRecupererElement = false;
  loadingChemin = '';

  constructor(private http: HttpClient, private route: ActivatedRoute,  private router: Router,private spinner: NgxSpinnerService ) {


  }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      
      this.chemin  = routeParams['ressource'];
      if (!this.chemin) {
        this.http.get(`${env.AppConfig.host}/config`).subscribe((data: any) => {
          this.chemin = data.dir ;
            
          });

      
      }
    });
    this.http.get(`${env.AppConfig.host}/model-courant`).subscribe((data: any) => {
      this.modelCourant = data.ressource ;
        
      });
  /*  this.http.get(`${env.AppConfig.host}/travail`).subscribe((dataDirTravail: any) => {
      this.chemin = dataDirTravail.dir;
    });
  */
  }
  explorer(elt: Element) {
    const nouveauChemin = this.chemin + '/' + elt.name;
    this.parent = { chemin: this.chemin, parent: this.parent };
    this.chemin = nouveauChemin;
    this.http.post(`${env.AppConfig.host}/lire`, nouveauChemin).subscribe((data: any) => {
      this.elements = [];
      if (data.type === 'directory') {
        this.elements = data.data;
      }
    });
  }
  explorerParent() {
    this.chemin = this.parent.chemin;
    this.parent = this.parent.parent;
    this.http.post(`${env.AppConfig.host}/lire`, this.chemin).subscribe((data: any) => {
      this.elements = [];
      if (data.type === 'directory') {
        this.elements = data.data;
      }
    });
  }
  nouveauRepertoire() {
    this.nouveau = { name: '', type: 'directory', valider: false };
  }
  nouveauFichier() {
    this.nouveau = { name: '', type: 'file', valider: false };
  }
  afficherValider() {
    this.nouveau.valider = false;
    var extensionOk = false;
    if (this.nouveau.type === "directory") {
      this.nouveau.valider = true;
    }
    this.extensions.forEach ( (extension:string ) => {
      if (this.nouveau.name.endsWith(extension)) {
        extensionOk = true;
      }
    })
    if (!extensionOk) {
      return;
    }
    this.http.post(`${env.AppConfig.host}/type`, this.chemin + '/' + this.nouveau.name).subscribe((data: any) => {

      if (!data.type) {
        this.nouveau.valider = true;
      }
    });
  }
  valider() {
    var contenu =' {} ';
    if (this.nouveau.name.endsWith('.model.json')) {
      contenu = '{"url":"application" , "apis":[] ,   "model": { "types":[]  }}'
    }
    this.http.post(`${env.AppConfig.host}/ecrire`, {
      ressource: `${this.chemin}/${this.nouveau.name}`,
      type: this.nouveau.type,
      contenu: contenu
    }
    ).subscribe((data: any) => {
      this.loadingChemin = '';
      // this.recharger();

    });

  }
  associer() {
    this.http.post(`${env.AppConfig.host}/model-courant`,{ ressource:this.chemin}).subscribe((data: any) => {
      
        
      });

  }
  ouvrirFichier(elt: Element) {
    const event = this.chemin + '/' + elt.name
      if (event.endsWith('.model.json')) {
        this.spinner.show();   
        this.http.post(`${env.AppConfig.host}/generer_client`, {
          ressource: event
        }).subscribe((data2:any)=> {

          this.spinner.hide();   
          this.router.navigate(['/editor/model', { chemin: this.chemin , nom:elt.name ,erreurs:JSON.stringify(data2.erreurs) , adminKey:data2.adminKey}]);       

        },(error:any)=> {
          this.spinner.hide();  
          this.router.navigate(['/editor/config', { chemin: this.chemin , nom:elt.name }]);       
          //alert("KO")
        })

      }
     
    }

  
  recupererElements(): Element [] {
    if (this.chemin !== this.loadingChemin) {
      this.loadingChemin = this.chemin;
      this.http.post(`${env.AppConfig.host}/lire`, this.chemin).subscribe((data: any) => {
        this.elements = [];
        this.erreurRecupererElement = true;
        if (data.type === 'directory') {
          this.elements = data.data;
          this.elements = this.elements.filter( (e: Element) => {
            if (e.type === 'directory') {
              return true;
            }
            for( let extension of this.extensions) {
              if  (e.name.endsWith(extension)) {
                return true;
              };
            }
            return false;
          });
          this.nouveau = undefined;
          this.erreurRecupererElement = false;
        }
      });
  }
    return this.elements;
  }
  initialiserRepertoireTravail() {
    this.http.post(`${env.AppConfig.host}/config/dir`,{ dir: this.chemin }).subscribe((data: any) => {

    });

  }

  recharger() {
    this.http.post(`${env.AppConfig.host}/lire`, this.chemin).subscribe((data: any) => {
      this.elements = [];
      if (data.type === 'directory') {
        this.elements = data.data;
        this.nouveau = undefined;
      }
    });
  }
  supprimer(elt: Element) {
    this.elements = this.elements.filter((e) => e.name !== elt.name );
    this.http.post(`${env.AppConfig.host}/supprimer`, `${this.chemin}/${elt.name}`
    ).subscribe((data: any) => {


    });
  }
  generer() {
    this.spinner.show();   
    this.http.post(`${env.AppConfig.host}/generer_client`, {
      ressource: this.modelCourant ,
      cible:this.chemin
    }).subscribe((data2:any)=> {

      this.spinner.hide();   
      
   
    },(error:any)=> {
      this.spinner.hide();  
      //alert("KO")
    })

  }
}
