import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditorComponent } from './editor/editor.component';
import { SelectComponent } from './select/select.component';
import { ExplorateurComponent } from './explorateur/explorateur.component';
import { CrudComponent } from './crud/crud.component';
import { CreateComponent } from './create/create.component';
import { ApiRestListComponent } from './api-rest-list/api-rest-list.component';
import { ModelComponent } from './model/model.component';
import { ConfigComponent } from './config/config.component';


const routes: Routes = [
  { path: 'editor', component: EditorComponent , children: [
      { path:'model' , component: ModelComponent },
       { path:'api' , component: ApiRestListComponent },
       { path: 'config', component: ConfigComponent }

  ]},
  { path: 'api', component: ApiRestListComponent },
  { path: 'crud', component: CrudComponent , children:[
       { path:'retrieve' , component:SelectComponent,pathMatch: 'full'},
       { path:'create' , component:CreateComponent,pathMatch: 'full'}

  ]},
  { path: 'explorer', component: ExplorateurComponent },
  
  { path: '',redirectTo: '/explorer', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
