import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-json-array',
  templateUrl: './json-array.component.html',
  styleUrls: ['./json-array.component.css']
})
export class JsonArrayComponent implements OnInit {
  @Input()
  array: any []
  open = true;
  constructor() { }

  ngOnInit() {
  }
  toggle() {
    this.open = ! this.open;
  }
  label() {
    if (this.open) {
      return "-";
    }
    return "+";
  }
  keys(): number [] {
      const r:number [] = []
      for(var i=0;i < this.array.length;i++){
        r.push(i);
      }
      return r;
  }
  isArray( obj:any): boolean {
    return Array.isArray(obj);
  }
  isObject( obj:any): boolean {
    return  (typeof obj !== "number" && typeof obj !== "boolean" && typeof obj !== "string" && !Array.isArray(obj))
  }
  isNumber( obj:any):boolean {
    return typeof obj === "number";
  }
  isBoolean( obj:any):boolean {
    return typeof obj === "boolean";
  }
  isString( obj:any):boolean {
    return typeof obj === "string";
  }
}
