import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as env from '../../environments/environment';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {

  host: string
  query: string
  nom: string
  typeQuery: string ="deep"
  mapQuery: Map<string, string> = new Map();
  historique: string[] = []
  selectionHistorique: string
 // options = new JsonEditorOptions();
 
  data:any []
  chemin: string;



  constructor(private http: HttpClient,private route: ActivatedRoute) {
/*
    this.options.mode = 'code';
    this.options.modes = ['code', 'text', 'tree', 'view'];
     this.options.statusBar = false;
  */
    }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.chemin = routeParams['chemin'];
      console.log(this.chemin);
      
    });
  }
  executer() {
    this.mapQuery.set(this.nom, this.query);
    this.historique = []
    this.mapQuery.forEach((value: string, key: string) => {
      this.historique.push(key);
    })
    console.log( this.typeQuery)
    const question= {
      query:this.query,
      host:this.host,
      deep: this.typeQuery === "deep"
    }
    this.http.post(env.AppConfig.host+"/select",JSON.stringify(question)).subscribe ( (data:any [])=> {
      this.data = data;
    })

  }
  select( value) {
      if (this.selectionHistorique) {
        this.mapQuery.set(this.selectionHistorique,this.query);
      }

      this.query =  this.mapQuery.get(value);
      this.nom = value;
  }

}
