import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import * as env from '../../environments/environment';
@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css']
})
export class ConfigComponent implements OnInit {
  javaJvm: string
  jpaClassPath: string
  keyEnvironement: string
  serveurEnvironement: string
  messageConfigJava: string
  messageConfigEnvironement: string
  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) {
    this.http.get(`${env.AppConfig.host}/config`).subscribe((data: any) => {


      this.javaJvm = data.jvm
      this.jpaClassPath = data.jpaClassPath;
      this.keyEnvironement = data.keyEnvironement;
      this.serveurEnvironement = data.serveurEnvironement
    });
  }
  modifierConfigJava() {
    this.http.post(`${env.AppConfig.host}/config/java`, {
      jvm: this.javaJvm,
      jpaClassPath: this.jpaClassPath
    }).subscribe((data: any) => {

      this.messageConfigJava = data.message

    }, (error: any) => {
      this.messageConfigJava = error.message

    });
  }
  modifierConfigEvironement() {
    this.http.post(`${env.AppConfig.host}/config/environement`, {
      keyEnvironement: this.keyEnvironement,
      serveurEnvironement: this.serveurEnvironement
    }).subscribe((data: any) => {

      this.messageConfigEnvironement = data.message

    }, (error: any) => {
      this.messageConfigEnvironement = error.message

    });
  }
  ngOnInit() {
  }

}
