import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiRestListComponent } from './api-rest-list.component';

describe('ApiRestListComponent', () => {
  let component: ApiRestListComponent;
  let fixture: ComponentFixture<ApiRestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApiRestListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiRestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
