import { Component, OnInit } from '@angular/core';
import { APIRest } from '../api-rest/api-rest.component';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import * as env from '../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
interface Erreur {
  nom:string ,
  libelle:string
}
@Component({
  selector: 'app-api-rest-list',
  templateUrl: './api-rest-list.component.html',
  styleUrls: ['./api-rest-list.component.css']
})
export class ApiRestListComponent implements OnInit {
  apiRestList:APIRest[];
  chemin: string;
  nom: string;
  url: string;
  ressource: string;
  obj: any;
  erreurs: Erreur[];
  constructor(private http: HttpClient , private route: ActivatedRoute,  private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.chemin = routeParams['chemin'];
      const nom = routeParams['nom'];
      const event = this.chemin + '/' + nom;
      this.erreurs = null;
      this.charger(event);
    });

  }
  ajouterErreurSiBesoin( apiRest: APIRest) : APIRest{
    if (!this.erreurs) {
      return apiRest;
    }
    const erreur:Erreur = this.erreurs.find( (e)=> { return e.nom === apiRest.nom});
    if (erreur) {
      apiRest.erreur = erreur.libelle;
    } else {
      apiRest.erreur = undefined;
    }
    return apiRest;
  }

  supprimer( apiRest: APIRest) {

      this.apiRestList = this.apiRestList.filter( (ar:APIRest)=> ar !== apiRest);
  }
  nouveau() {
    const apiRest:APIRest = { nom:"" ,requete:"" , useIndex:false }
    this.apiRestList.push(apiRest);
  }
  charger(ressource: string) {
    
    this.ressource = ressource;
    this.spinner.show();
    this.http.post(`${env.AppConfig.host}/lire`, ressource).subscribe(
      (data: any) => {
        const ls: any[] = []
        if (!data.data) {
            this.apiRestList = [];

            return;
        }
        
        this.obj = JSON.parse(data.data);
        this.apiRestList = this.obj.apis;
        this.url = this.obj.url;
        this.http.post(`${env.AppConfig.host}/update_api`, {
          ressource: this.ressource
        }).subscribe((data2:any)=> {
          console.log("resultat" + data2.erreurs)
          this.erreurs = data2.erreurs;
          this.spinner.hide();
          
  
        } ,(error: any)=> {
          this.router.navigate(['/editor/config', { chemin: this.chemin , nom:this.nom }]);     
          this.spinner.hide();
        })
        
      }
    )
  }
  enregistrer() {
    var operation: string = "update_api";
    if (this.obj) {
       this.obj.apis = this.apiRestList; 
       if (this.obj.url !== this.url) {
         operation = 'generer_client'
       }

       this.obj.url = this.url;
    } else {
      this.obj = {url:this.url , apis:this.apiRestList , model: {}}
    }
    this.apiRestList.forEach( (api)=> {
      api.erreur = null;
    })
    this.spinner.show();
    this.http.post(`${env.AppConfig.host}/ecrire`, {
      ressource: this.ressource,
      type: 'file',
      contenu: JSON.stringify(this.obj)
    }).subscribe((data: any) => { 
      this.http.post(`${env.AppConfig.host}/${operation}`, {
        ressource: this.ressource
      }).subscribe((data2:any)=> {
        console.log("resultat" + data2.erreurs)
        this.erreurs = data2.erreurs;
        this.spinner.hide();
        

      })


    });
  }


}
